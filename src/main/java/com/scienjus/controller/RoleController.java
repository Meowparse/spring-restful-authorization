package com.scienjus.controller;

import com.alibaba.fastjson.JSONObject;
import com.scienjus.annotation.ULog;
import com.scienjus.authorization.annotation.Authorization;
import com.scienjus.dao.SysPermissionRoleDao;
import com.scienjus.dao.SysRoleUserDao;
import com.scienjus.domain.SysPermission;
import com.scienjus.model.PermissionModel;
import com.scienjus.model.ResultModel;
import com.scienjus.repository.SysPermissionRepository;
import com.scienjus.service.SysPermissionRoleService;
import com.scienjus.service.SysRoleService;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by MәӧωρaЯsε on 2017/6/19.
 *
 */
@Controller
@RequestMapping("role")
public class RoleController {

    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private SysRoleService sysRoleService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResultModel> getRole(){
        JSONObject jsonModel = new JSONObject();
        jsonModel.put("role","hello role");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRole", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiOperation(value = "获得角色权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity getRolePermission(@RequestParam int roleId){
        ResultModel resultModel = sysRoleService.getRolePermission(roleId);
        return new ResponseEntity<>(ResultModel.ok(resultModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "设置角色权限")
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity setRolePermission(@RequestParam int roleId,@RequestParam String permission){
        ResultModel resultModel = sysRoleService.updateRolePermission(permission,roleId);
        return new ResponseEntity<>(resultModel, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "删除角色")
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity deleteRole(@RequestParam int roleId){
        sysRoleService.deleteRole(roleId);
        return new ResponseEntity<>(ResultModel.ok(true), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "新增角色")
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity createRole(@RequestParam String roleName){
        ResultModel resultModel = sysRoleService.createRole(roleName);
        return new ResponseEntity<>(resultModel, HttpStatus.OK);
    }

}
