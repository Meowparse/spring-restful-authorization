package com.scienjus.controller;

import com.alibaba.fastjson.JSONObject;
import com.scienjus.authorization.annotation.Authorization;
import com.scienjus.model.ResultModel;
import com.scienjus.service.SysUserService;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by MәӧωρaЯsε on 2017/5/3.
 *
 */
@RestController
@RequestMapping("/blog")
public class BlogController {

    private static final Logger logger = LoggerFactory.getLogger(BlogController.class);

    @Autowired
    SysUserService sysUserService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResultModel> getBlog(){
        JSONObject jsonModel = new JSONObject();
        jsonModel.put("blog","hello getBlog");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> getList(){
        JSONObject jsonModel = new JSONObject();
        jsonModel.put("blog","hello getList");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/one", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> getOne(){
        JSONObject jsonModel = new JSONObject();
        jsonModel.put("blog","getOne");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> update(){

        JSONObject jsonModel = new JSONObject();
        jsonModel.put("blog","update");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

}
