package com.scienjus.controller;

import com.alibaba.fastjson.JSONObject;
import com.scienjus.authorization.annotation.Authorization;
import com.scienjus.model.ResultModel;
import com.scienjus.service.SysPermissionService;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author MәӧωρaЯsε
 * @date 2017/6/29.
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private SysPermissionService sysPermissionService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResultModel> getExport() {
        JSONObject jsonModel = new JSONObject();
        jsonModel.put("permission", "hello permission");
        return new ResponseEntity<>(ResultModel.ok(jsonModel), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> create(@RequestParam String name, @RequestParam String des,
                                              @RequestParam String url, @RequestParam int pid, @RequestParam String method) {

        ResultModel resultModel = sysPermissionService.createPermission(name, des, url, pid, method);

        return new ResponseEntity<>(resultModel, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> delete( @RequestParam int id) {

        ResultModel resultModel = sysPermissionService.deletePermission(id);

        return new ResponseEntity<>(resultModel, HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = "authorization", required = true, dataType = "string", paramType = "header"),
    })
    public ResponseEntity<ResultModel> update(@RequestParam int id,@RequestParam String name, @RequestParam String des,
                                              @RequestParam String url, @RequestParam int pid, @RequestParam String method){

        ResultModel resultModel = sysPermissionService.updatePermission(id,name,des,url,pid,method);

        return new ResponseEntity<>(resultModel,HttpStatus.OK);
    }

}
