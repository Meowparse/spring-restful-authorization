package com.scienjus.service;

import com.scienjus.domain.SysPermission;

import java.util.List;

/**
 * Created by MәӧωρaЯsε on 2017/5/5.
 *
 */
public interface SysPermissionRoleService {

    /**
     * 获得某个角色的权限
     * @param roleId 角色id
     * @return list
     */
    List<SysPermission> getRolePermission(int roleId);

}
