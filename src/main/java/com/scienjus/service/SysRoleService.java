package com.scienjus.service;


import com.scienjus.domain.SysRole;
import com.scienjus.model.PermissionModel;
import com.scienjus.model.ResultModel;

import java.util.List;

/**
 * Created by MәӧωρaЯsε on 2017/6/21.
 *
 */
public interface SysRoleService {

    /**
     * 获得角色权限
     * @param roleId  角色ID
     * @return list
     */
    ResultModel getRolePermission(int roleId);

    /**
     * 更新角色权限
     * @param pIds 权限ID
     * @param roleId 角色ID
     * @return
     */
    ResultModel updateRolePermission(String pIds,int roleId);

    /**
     * 新增角色
     * @param roleName 角色名
     * @return
     */
    ResultModel createRole(String roleName);

    /**
     * 删除角色
     * @param roleId 角色ID
     */
    ResultModel deleteRole(int roleId);

}
