package com.scienjus.service;

import com.scienjus.domain.SysPermission;
import com.scienjus.model.PermissionModel;
import com.scienjus.model.ResultModel;

import java.util.List;
import java.util.Map;

/**
 * Created by MәӧωρaЯsε on 2017/5/5.
 */
public interface SysPermissionService {

    /**
     * 获得某个用户的权限
     *
     * @param id 用户ID
     * @return list
     */
    List<SysPermission> getPermissionByUserId(int id);

    /**
     *
     * @param id
     * @return list
     */
    List<Integer> findAllById(int id);

    /**
     * 新增权限
     *
     * @param name   名称
     * @param des    描述
     * @param url    请求地址
     * @param pid    父ID 一级为(0)
     * @param method 请求方式
     * @return ResultModel
     */
    ResultModel createPermission(String name, String des, String url, int pid, String method);

    /**
     * 删除权限
     *
     * @param id
     * @return
     */
    ResultModel deletePermission(int id);

    /**
     * 更新权限
     *
     * @param id     权限ID
     * @param name   名称
     * @param des    描述
     * @param url    请求地址
     * @param pid    父ID
     * @param method 请求方式
     * @return ResultModel
     */
    ResultModel updatePermission(int id, String name, String des, String url, int pid, String method);

}
