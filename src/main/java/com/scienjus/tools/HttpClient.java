package com.scienjus.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Studio on 2017/1/6.
 * author fj
 */
public class HttpClient {

    private static final Logger logger =  LoggerFactory.getLogger(HttpClient.class);
    //post 请求
    public static String sendPost(String url,String param){
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try
        {
            URL realUrl = new URL(url);
            //打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            //设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

            //发送POST请求必须设置如下两行
            conn.setDoOutput(Global.TRUE);
            conn.setDoInput(Global.TRUE);
            //获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            //发送请求参数
            out.print(param);
            //flush输出流的缓冲
            out.flush();
            //定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(),"utf-8"));//全文编码保持一致
            String line;
            while ((line = in.readLine())!= null)
            {
                result += line;
            }
        }
        catch(Exception e)
        {
            //System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
            logger.debug("HttpClient:sendPost:exception{}" + e.toString());
        }
        //使用finally块来关闭输出流、输入流
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
                if (in != null)
                {
                    in.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                logger.debug("HttpClient:sendPost:exception{}" + ex.toString());
            }
        }
        return result;
    }

    public static String HashMap2String(HashMap<String,String> param){
        String tmp = "";
        Iterator iter = param.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String)entry.getKey();
            String val = (String)entry.getValue();

            tmp+=key+"="+val+"&";
        }
        if(tmp!="")
        {
            tmp=tmp.substring(0,tmp.length()-1);
        }
        return tmp;
    }

}
