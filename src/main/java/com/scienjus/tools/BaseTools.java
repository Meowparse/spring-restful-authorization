package com.scienjus.tools;

import java.util.*;

/**
 * @author MәӧωρaЯsε
 * @date 2017/6/29.
 */
public class BaseTools {

    public static List<Integer> strToList(String str){
        List<Integer> ids = new ArrayList<>();
        for (String permissionId : str.split(",")) {
            ids.add(Integer.parseInt(permissionId));
        }

        return ids;
    }

    public static Set<Integer> strToSet(String str){
        Set<Integer> ids = new HashSet<>();
        for(String s : str.split(","))
            ids.add(Integer.parseInt(s));

        return ids;
    }

    public static List<String> intListToStrList(List <Integer> list){
        List<String> strList = new ArrayList<>();
        for (int i : list){
            strList.add(i+"");
        }
        return strList;
    }

    //去重
    public static String[] unique(String[] arrs){
        Set<String> set = new HashSet<>();
        String[] strArr = new String[0];
        Collections.addAll(set, arrs);
        return set.toArray(strArr);
    }
}
