package com.scienjus.tools;


/**
 * Created by studio on 2017/6/27.
 */
public class Global {
    //为了减少布尔类带来的内存分配
    public static final Boolean TRUE = Boolean.valueOf("true");
    public static final Boolean FALSE = Boolean.valueOf("false");

}
