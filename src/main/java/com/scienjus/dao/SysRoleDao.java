package com.scienjus.dao;

import com.scienjus.domain.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by MәӧωρaЯsε on 2017/6/21.
 *
 */
public interface SysRoleDao extends JpaRepository<SysRole,Integer>{

    @Query(value = "update sys_permission_role set permission_id = ? where role_id = ?",nativeQuery = true)
    @Modifying
    @Transactional
    void updateRolePermission(String permissions,int roleId);

    SysRole findByName(String roleName);

}
