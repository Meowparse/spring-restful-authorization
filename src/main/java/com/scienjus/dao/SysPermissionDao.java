package com.scienjus.dao;

import com.scienjus.domain.SysPermission;
import com.scienjus.model.PermissionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

/**
 * Created by MәӧωρaЯsε on 2017/5/5.
 *
 */
public interface SysPermissionDao extends JpaRepository<SysPermission,Integer>{

    @Query(value = "SELECT permission_id FROM sys_permission_role WHERE id = ?",nativeQuery = true)
    String findAllById(int id);

    @Query(value = "SELECT group_concat(permission_id separator ',') FROM sys_permission_role " +
            "WHERE role_id in (select sys_role_id from sys_role_user where sys_user_id = ?) ",nativeQuery = true)
    String findAllByUserId(int id);

//    @Query(value = "SELECT * FROM sys_permission_role WHERE id in ( ? )",nativeQuery = true)
    List<SysPermission> findByIdIn(Collection ids);

    List<SysPermission> findByNameOrUrl(String name,String url);

}
