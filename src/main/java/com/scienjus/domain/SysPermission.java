package com.scienjus.domain;

import javax.persistence.*;

/**
 * Created by MeowParse on 2017/5/2.
 *
 */
@Entity
@Table(name = "sys_permission")
public class SysPermission {


    private Integer id;
    private String name;
    private String description;
    private String url;
    private Integer pid;
    private String method;


    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    //权限名称
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //权限描述
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //授权链接
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    //父节点id
    @Column(name = "pid")
    public Integer getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    //请求方法
    @Column(name = "method")
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

}
