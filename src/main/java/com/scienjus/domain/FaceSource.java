package com.scienjus.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author MәӧωρaЯsε
 * @date 2017/7/10.
 */
@Entity
@Table(name = "face_source")
public class FaceSource {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;
    @Column(name = "src_id")
    private String srcId;
    @Column(name = "description")
    private String description;
    @Column(name = "config")
    private String config;
    @Column(name = "visit_time")
    private Date visitTime;
    @Column(name = "subid")
    private String subId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrcId() {
        return srcId;
    }

    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }

    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }


    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

}
