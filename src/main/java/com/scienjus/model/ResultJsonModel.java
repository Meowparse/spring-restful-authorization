package com.scienjus.model;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MәӧωρaЯsε
 * @date 2017/7/7.
 */
public class ResultJsonModel {

    @JSONField(name="code")
    private String code;
    @JSONField(name="msg")
    private String msg;
    @JSONField(name="results")
    private JSONObject results;










    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JSONObject getResults() {
        return results;
    }

    public void setResults(JSONObject results) {
        this.results = results;
    }









}
