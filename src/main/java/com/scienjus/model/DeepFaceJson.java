package com.scienjus.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.scienjus.config.DeepFaceConstants;


/**
 * @author MәӧωρaЯsε
 * @date 2017/7/7.
 */
public class DeepFaceJson {

    private JSONObject deepFaceJson = new JSONObject();
    private JSONObject params = new JSONObject();
    private JSONObject typeJson = new JSONObject(); //new

    public DeepFaceJson(int method) {
        deepFaceJson.put("method", DeepFaceConstants.DEEP_FACE_METHOD.get(method));
        params.put("appkey", DeepFaceConstants.APPKEY);
        deepFaceJson.put("params", params);
        if (DeepFaceConstants.OP_TYPE.containsKey(method)) {
            deepFaceJson.put(DeepFaceConstants.OP_TYPE.get(method), typeJson);
        }
    }


    public DeepFaceJson(int method, String var1) {
        this(method);
        switch (method) {
            case 3:
                setDes(var1);
                break;
            case 4:
            case 7:
            case 12:
                setAId(var1);
                break;
            case 9:
            case 11:
            case 15:
                setDId(var1);
                break;
            case 14:
            case 17:
            case 18:
                setSid(var1);
                break;
            case 21:
            case 24:
            case 26:
            case 43:
            case 45:
            case 47:
                setId(var1);
                break;
            case 33:
            case 34:
            case 42:
                setFid(var1);
                break;
            case 36:
                setVideo(var1);
                break;
            case 37:
                setTid(var1);
                break;
            case 44:
                setImg(var1);
                break;
        }

    }

    public DeepFaceJson(int method, String var1, String var2) {
        this(method);
        switch (method) {

            case 2:
                setDesAndCon(var1, var2);
                break;
            case 4:
            case 5:
                setAreaIdAndDes(var1, var2);
                break;
            case 25:
            case 46:
                setIdAndImg(var1, var2);
                break;
            case 27:
            case 41:
                setFidAndPid(var1, var2);
                break;
            case 28:
            case 30:
            case 31:
            case 48:
            case 51:
                setIdAndSids(var1, var2);
                break;
            case 29:
            case 32:
                setFidAndTid(var1, var2);
                break;
            case 35:
                setFidAndImg(var1, var2);
                break;
            case 49:
            case 52:
                setPidAndTid(var1, var2);
                break;
            case 50:
                setId1AndId2(var1, var2);
                break;
        }
    }

    public DeepFaceJson(int method, int var1, int var2) {
        this(method);
        switch (method) {
            case 20:
                setCount(var1, var2);
                break;
            case 23:
            case 38:
                setBidAndCount(var1, var2);
                break;
        }
    }

    public DeepFaceJson(int method, int var1) {
        this(method);
        switch (method) {
            case 19:
                setCount(var1);
                break;
            case 39:
                setSex(var1);
                break;

        }
    }

    public DeepFaceJson(int method, String var1, int var2) {
        this(method);
        switch (method) {
            case 40:
                setIdAndSex(var1, var2);
                break;
        }
    }

    public DeepFaceJson(int method, boolean glasses, int glassesId, boolean hair, int hairId , String img) {
        this(method);
        JSONObject style = new JSONObject(); //new
        style.put("glasses", glasses);
        style.put("glasses_id", glassesId);
        style.put("hair", hair);
        style.put("hair_id", hairId);
        params.put("style", style);
        params.put("img",img);

    }

    public DeepFaceJson(int method, int type, String id, String path) {
        this(method);
        params.put("type", type);
        params.put("id", id);
        params.put("path", path);

    }

    public DeepFaceJson(int method, String var1, String var2, String var3) {
        this(method);
        switch (method) {
            case 8:
                setAreaIdVStrAndDes(var1, var2, var3);
                break;
            case 10:
                setDidVStrAndDes(var1, var2, var3);
                break;
        }

    }

    public DeepFaceJson(int method, String var1, String var2, String var3, String var4) {
        this(method);
        switch (method) {
            case 13:
            case 14:
            case 16:
                setDidSubidDesAndCon(var1, var2, var3, var4);
        }
    }

    private void setDesAndCon(String description, String config) {

        typeJson.put("description", description);
        typeJson.put("config", config);

    }

    private void setDes(String description) {
        typeJson.put("description", description);
    }

    private void setAId(String areaId) {
        params.put("id_area", areaId);
    }

    private void setAreaIdAndDes(String areaId, String description) {
        params.put("id_area", areaId);
        typeJson.put("description", description);
    }

    private void setAreaIdVStrAndDes(String areaId, String validString, String description) {
        params.put("id_area", areaId);
        typeJson.put("validstring", validString);
        typeJson.put("description", description);
    }

    private void setDId(String DeviceId) {
        params.put("id_device", DeviceId);
    }

    private void setDidVStrAndDes(String DeviceId, String validString, String description) {
        params.put("id_device", DeviceId);
        typeJson.put("validstring", validString);
        typeJson.put("description", description);
    }

    private void setDidSubidDesAndCon(String srcId, String subId, String description, String config) {
        params.put("id_src", srcId);
        typeJson.put("subid", subId);
        typeJson.put("description", description);
        typeJson.put("config", config);
    }

    private void setSid(String srcId) {
        params.put("id_src", srcId);
    }

    private void setCount(int count) {
        params.put("count", count);

    }

    private void setCount(int count, int matchedCount) {
        params.put("count", count);
        params.put("matched_count", matchedCount);

    }

    private void setId(String id) {
        params.put("id", id);
    }

    private void setBidAndCount(int beginIdx, int count) {
        params.put("begin_idx", beginIdx);
        params.put("count", count);
    }

    private void setIdAndImg(String id, String img) {
        params.put("id", id);
        params.put("img", img);
    }

    private void setFidAndPid(String faceTrackId, String personId) {
        params.put("id_facetrack", faceTrackId);
        params.put("id_person", personId);
    }

    private void setIdAndSids(String faceTrackId, String srcIds) {
        params.put("id", faceTrackId);

        JSONArray srcIdArr = JSON.parseArray(JSON.toJSONString(srcIds.split(",")));

        params.put("src_ids", srcIdArr);
    }

    private void setFidAndTid(String faceTrackId, String transId) {
        params.put("id_facetrack", faceTrackId);
        params.put("id_trans", transId);
    }

    private void setFid(String faceTranckId) {
        params.put("id_facetrack", faceTranckId);
    }

    private void setFidAndImg(String faceTranckId, String img) {
        params.put("id_facetrack", faceTranckId);
        params.put("img", img);
    }

    private void setVideo(String video) {
        params.put("video", video);
    }

    private void setTid(String transactionId) {
        params.put("transaction_id", transactionId);
    }

    private void setSex(int sex) {
        typeJson.put("sex", sex);
    }

    private void setIdAndSex(String id, int sex) {
        typeJson.put("sex", sex);
        params.put("id", id);
    }

    private void setImg(String img) {
        params.put("img", img);
    }

    private void setPidAndTid(String personId, String transId) {
        params.put("id_person", personId);
        params.put("id_trans", transId);
    }

    private void setId1AndId2(String id1, String id2) {
        params.put("id1", id1);
        params.put("id2", id2);
    }

    public String toString() {
        return deepFaceJson.toString();
    }

    public JSONObject getDeepFaceJson(){
        return  deepFaceJson;
    }

}
