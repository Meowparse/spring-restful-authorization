package com.scienjus.config;

/**
 * 自定义请求状态码
 *
 * @author ScienJus
 * @date 2015/7/15.
 */
public enum ResultStatus {
    SUCCESS(100, "成功"),
    USERNAME_OR_PASSWORD_ERROR(-1001, "用户名或密码错误"),
    USER_NOT_FOUND(-1002, "用户不存在"),
    USER_NOT_LOGIN(-1003, "用户未登录"),
    PERMISSION_DENIED(-1004, "未授权"),
    //role
    ROLENAME_EXIST(-2001, "角色名已存在"),
    ROLE_NOTEXIST(-2003, "角色不存在"),
    ROLE_DENIED(-2002, "没有权限"),
    //permission
    PERMISSON_EXIST(-3001, "权限名称或者URL已存在"),
    PERMISSON_NOTEXIST(-3002, "权限不存在");
    /**
     * 返回码
     */
    private int code;

    /**
     * 返回结果描述
     */
    private String message;

    ResultStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
