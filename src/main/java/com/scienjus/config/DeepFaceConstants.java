package com.scienjus.config;

import java.util.*;

/**
 * @author MәӧωρaЯsε
 * @date 2017/7/7.
 */
public class DeepFaceConstants {

    public static final String URL = "http://";

    public static final String APPKEY = "APPKEY";

    public static final HashMap<Integer, String> OP_TYPE = new HashMap<Integer, String>(){
        {
            put(5, "value");
            put(10, "value");
            put(16, "value");

            put(2, "info");
            put(3, "info");
            put(8, "info");
            put(13, "info");
            put(39, "info");
            put(40, "info");
        }
    };


    public static final HashMap<Integer, String> DEEP_FACE_METHOD = new HashMap<Integer, String>(){
        {
            put(1,  "getappinfo");  //获取APP的参数和设置信息
            put(2,  "updateappinfo");    //更新APP信息

            put(3,  "createarea");    //创建新区域
            put(4,  "deletearea");    //删除区域
            put(5,  "updatearea");    //更新区域信息
            put(6,  "getareas");    //获取App的区域列表
            put(7,  "getareainfo");        //获取区域信息

            put(8,  "createdevice");        //创建设备
            put(9,  "deletedevice");    //删除设备
            put(10, "updatedevice");    //更新设备
            put(11, "getdeviceinfo");    //获取设备信息
            put(12, "getdevices");    //获取区域包含的设备列表

            put(13, "createsource");    //创建视频源
            put(14, "deletesource");    //删除视频源
            put(15, "getsources");    //获取设备下属的视频源列表
            put(16, "updatesource");    //更新视频源信息
            put(17, "getsourceinfo");    //获取视频源信息
            put(18, "changesourcestate");    //更新视频源状态（启动、停止切换）

            put(19, "getunprocessedfacetrack");    //获取未处理的FaceTrack列表
            put(20, "getunprocessedmatchedfacetrack");    //获取未处理但已自动匹配的FaceTrack列表
            put(21, "ackfacetrack");    //确认接收FaceTrack
            put(22, "ackallfacetrack");    //确认接收所有FaceTrack
            put(23, "getfacetrack");    //获取FaceTrack列表
            put(24, "getfacetrackinfo");    //获取FaceTrack详细信息
            put(25, "deletefacetrackimg");    //删除FaceTrack中的某个图片
            put(26, "deletefacetrack");    //删除FaceTrack
            put(27, "matchfacetrack2singleperson");    //FaceTrack与Person一对一匹配
            put(28, "matchfacetrack2person");    //在Person中匹配FaceTrack
            put(29, "getmatchfacetrackresult");    //获取FaceTrack匹配结果
            put(30, "matchfacetrack2person_sync");    //在Person中匹配FaceTrack（同步）
            put(31, "matchfacetrack2facetrack");    //在其他FaceTrack集合中匹配某一FaceTrack
            put(32, "getmatchfacetrack2facetrackresult");    //获取上一匹配的结果
            put(33, "createfacetrack");    //创建FaceTrack
            put(34, "createfacetrack_id");    //创建FaceTrack（指定id）
            put(35, "addimgtofacetrack");    //添加图像到FaceTrack
            put(36, "createfacetrackfromvideo");    //输入小视频创建FaceTrack
            put(37, "getfacetrackidinvideo");    //查询视频提取的FaceTrack结果

            put(38, "getperson");    //获取所有Person
            put(39, "createperson");    //创建Person
            put(40, "createperson_id");    //创建Person（输入ID标识）
            put(41, "addfacetracktoperson");    //将FaceTrack图像导入Person
            put(42, "cancelfacetrackfromperson");    //撤销导入Person的FaceTrack
            put(43, "getpersonrelatedfacetracks");    //获取被添加到Person的FaceTrack列表
            put(44, "addimgtoperson");    //添加单个人脸图像到Person
            put(45, "getpersoninfo");    //获取Person信息
            put(46, "deletepersonimg");    //删除Person中特定图像文件
            put(47, "deleteperson");    //删除Person
            put(48, "matchperson2facetrack");    //在FaceTrack中匹配Person
            put(49, "getmatchpersonresult");    //获取上一匹配结果
            put(50, "mergepersons");    //合并Person
            put(51, "matchperson2person");     //在Person库中检索与某一Person相似的其他Person
            put(52, "getmatchperson2personresult");    //获取上一匹配的结果
            put(53, "cropface");    //处理用户上传人脸图像
            put(54, "getsingleimg");    //获取图像（FaceTrack、Person、视频源等）
            put(55, "notify");    //消息提示接口

        }
    };
}
