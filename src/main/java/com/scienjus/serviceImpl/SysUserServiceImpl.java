package com.scienjus.serviceImpl;

import com.scienjus.dao.SysUserDao;
import com.scienjus.domain.SysUser;
import com.scienjus.model.ResultModel;
import com.scienjus.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by MәӧωρaЯsε on 2017/5/5.
 * 用户
 */
@Service
public class SysUserServiceImpl implements SysUserService{

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public SysUser getUserByUsername(String username) {
        return sysUserDao.findByUsername(username);
    }

    @Override
    public void updateNicknameByUserid(String username,Long userId) {
         sysUserDao.updateNickname(username,userId);
    }

    @Override
    public ResultModel getMenu() {
        return null;
    }

}
