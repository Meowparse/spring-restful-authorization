package com.scienjus.serviceImpl;

import com.scienjus.config.ResultStatus;
import com.scienjus.dao.SysPermissionDao;
import com.scienjus.domain.SysPermission;
import com.scienjus.model.PermissionModel;
import com.scienjus.model.ResultModel;
import com.scienjus.service.SysPermissionService;
import com.scienjus.tools.BaseTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by MәӧωρaЯsε on 2017/5/5.
 * 权限
 */
@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    private static final Logger logger = LoggerFactory.getLogger(SysPermissionServiceImpl.class);

    @Autowired
    private SysPermissionDao sysPermissionDao;

    @Override
    public List<SysPermission> getPermissionByUserId(int id) {
        String pIds = sysPermissionDao.findAllByUserId(id);
        System.out.println(pIds);
        Set<Integer> set = new HashSet<>();

        for (String s : pIds.split(","))
            set.add(Integer.parseInt(s));
        System.out.println(Arrays.toString(set.toArray()));
        logger.info(Arrays.toString(sysPermissionDao.findByIdIn(set).toArray()));
        return sysPermissionDao.findByIdIn(set);
    }

    @Override
    public List<Integer> findAllById(int id) {
        String pIds = sysPermissionDao.findAllById(id);
        return BaseTools.strToList(pIds);
    }

    @Override
    public ResultModel createPermission(String name, String des, String url, int pid, String method) {

        SysPermission sysPermission = new SysPermission();
        sysPermission.setName(name);
        sysPermission.setDescription(des);
        sysPermission.setUrl(url);
        sysPermission.setPid(pid);
        sysPermission.setMethod(method);

        if (!validata(sysPermission)) {
            return ResultModel.error(ResultStatus.PERMISSON_EXIST);
        }
        sysPermissionDao.save(sysPermission);
        return ResultModel.ok();

    }

    @Override
    public ResultModel deletePermission(int id) {

        sysPermissionDao.delete(id);
        return ResultModel.ok();
    }

    @Override
    public ResultModel updatePermission(int id, String name, String des, String url, int pid, String method) {

        SysPermission sysPermission = sysPermissionDao.getOne(id);
        if (sysPermission == null) {
            return ResultModel.error(ResultStatus.PERMISSON_NOTEXIST);
        }

        sysPermission.setName(name);
        sysPermission.setDescription(des);
        sysPermission.setUrl(url);
        sysPermission.setPid(pid);
        sysPermission.setMethod(method);

        if (!validata(sysPermission)) {
            return ResultModel.error(ResultStatus.PERMISSON_EXIST);
        }

        sysPermissionDao.save(sysPermission);

        return ResultModel.ok();
    }

    private boolean validata(SysPermission sysPermission) {

        List<SysPermission> sysPermissions = sysPermissionDao.findByNameOrUrl(sysPermission.getName(),
                sysPermission.getUrl());
        return sysPermissions.size() <= 0;
    }

}
